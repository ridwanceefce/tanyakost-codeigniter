<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Error extends CI_Controller {

  public function index()
  {
    $this->load->helper('url');
    $data['judul'] = "Halaman tidak ditemukan";
    $this->load->view('v_error404.php', $data);
  }

}

/* End of file Error.php */
/* Location: ./application/controllers/Error.php */