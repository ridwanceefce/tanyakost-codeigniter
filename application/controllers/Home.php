<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

  function __construct()
  {
    parent:: __construct();
    // $this->load->model('Main_m'); // untuk meload model hanya sekali saja
    $this->load->helper('form');
  }

  public function index()
  {
    $data['judul'] = "Tanyakost.com | Tempat cari kost";
    $this->load->view('v_home_page', $data);
  }

  public function cari_kost() {
    $keyword = $this->input->get('q');

    $this->load->model('kost_m');

    $kosts = $this->kost_m->ambil_data_cari_kost($keyword)->result();

    if($keyword){
      $data['keyword']  = $keyword;
    } else {
      $data['keyword']  = "Semua Kost"; // jika keyword kosong maka akan menampilkan semua kost di view
    }

    $data['kosts']    = $kosts;
    $data['judul']    = "Tanyakost.com | Tempat cari kost";

    $this->load->view('v_kost_hasil', $data);
  }

  public function cari_detail() { 
    $id_kost = $this->uri->segment(2);

    $this->load->helper('form');
    $this->load->helper('date');
    
    $this->load->model('kost_m');
    $this->load->model('user_m');

    $kost     = $this->kost_m->ambil_kost_id($id_kost)->row_array();
    $id_user  = $kost['id_user'];
    $waktu_upload = $kost['tanggal_upload']; // untuk menambil nilai timestamp tanggal upload di tabel kost
    $now = time(); // menunjukan waktu sekarang

    $user     = $this->user_m->ambil_user($id_user);

    $data['time'] = timespan($waktu_upload, $now). ' ago'; // menggunakan function timespan, dari class date
    // untuk menampilkan waktu dengan format 1 years ago.
    $data['kost'] = $kost;
    $data['user'] = $user;
    $data['notif'] = $this->session->flashdata('notif');
    $data['judul'] = "Tanyakost.com | Tempat cari kost";

    $this->load->view('v_hasil_detail', $data);
  }

  public function tentang_kami() {
    $data['judul'] = "Tanyakost.com | Tempat cari kost";

    $this->load->view('v_tentang_kami', $data);
  }
}