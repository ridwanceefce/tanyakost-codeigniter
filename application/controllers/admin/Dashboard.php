<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

  public function __construct() {
    parent:: __construct();

    $this->load->model('Account_m');

    if(!$this->Account_m->validasi_cookie_admin()) {
      $this->session->set_flashdata('notif', 'Akses ditolak');
      redirect('login-user');
    }
  }

  public function index() {
    $id_admin = $this->session->userdata('c_id_ad'); // ambil id user dari session dengan key C_id (cookie id :D)

    $this->load->helper('text');

    $this->load->model('admin_m'); // load model admin untuk controller dashborard
    $this->load->model('user_m'); // load model user
    $this->load->model('kost_m'); // load model kost
    $this->load->model('pesan_m');

    $admin = $this->admin_m->ambil_semua_data_admin($id_admin); // simpan data admin di variabel $admin dalam bentuk array
    $users = $this->user_m->ambil_user_terbaru()->result();
    $kosts = $this->kost_m->ambil_kost_terbaru()->result();
    $pesans = $this->pesan_m->ambil_pesan_terbaru()->result();

    $data['no_user'] = 1;
    $data['no_pesan'] = 1;
    $data['pesans'] = $pesans;
    $data['kosts']  = $kosts; // masukkan data object $kosts ke dalam array $data
    $data['users']  = $users; // masukkan data object $users kedalam array $data
    $data['admin']  = $admin; // masukkan data $admin kedalam array $data dengan key admin untuk dilmpar ke view
    $data['judul']  = "Tanyakost.com | Tempat cari kost";

    $this->load->view('admin/v_dashboard_admin', $data);
  }

  public function ubah_profile() {
    $id_admin =  $this->session->userdata('c_id_ad'); // ambil id user dari session dengan key C_id (cookie id :D)

    $this->load->helper('form');
    
    $this->load->model('admin_m');

    $admin = $this->admin_m->ambil_semua_data_admin($id_admin);

    $data['admin'] = $admin;
    $data['judul'] = "Tanyakost.com | Tempat cari kost";
    $this->load->view('admin/v_ubah_profil', $data);
  }

  public function ubah_profile_simpan() {
    $id_admin = $this->input->post('id_admin');

    $admin = array(
      'nama'  => $this->input->post('user-name'),
      'email' => $this->input->post('email-user'),
      'telepon' => $this->input->post('telp-user'),
      'password' => md5($this->input->post('password-user')) );

    $this->db->where('id', $id_admin);
    $this->db->update('admin', $admin);

    redirect('admin');
  }

}
