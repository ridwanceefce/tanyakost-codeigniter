<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kost extends CI_Controller {
  
  public function __construct() {
    parent:: __construct();

    $this->load->model('Account_m');

    if(!$this->Account_m->validasi_cookie_admin()) {
      $this->session->set_flashdata('notif', 'Akses ditolak');
      redirect('login-user');
    }
  }

  public function index() 
  {
    $id_admin = $this->session->userdata('c_id_ad'); // ambil id user dari session dengan key C_id (cookie id :D)

    $this->load->model('kost_m');
    $this->load->model('admin_m');

    $admin = $this->admin_m->ambil_semua_data_admin($id_admin);
    $kosts = $this->kost_m->ambil_semua_kost()->result();

    $data['admin'] = $admin;
    $data['kosts'] = $kosts;
    $data['judul'] = "Tanyakost.com - Semua Kost"; // Data yang tampil di title page

    $this->load->view('admin/v_tampil_kost', $data);
  }

  public function detail_kost() {
    $id_admin = $this->session->userdata('c_id_ad'); // ambil id user dari session dengan key C_id (cookie id :D)
    $id_kost  = $this->uri->segment(3);

    $this->load->model('kost_m');
    $this->load->model('admin_m');

    $admin  = $this->admin_m->ambil_semua_data_admin($id_admin);
    $kost   = $this->kost_m->ambil_kost_detail($id_kost)->row_array();

    $data['admin']  = $admin;
    $data['kost']   = $kost;
    $data['judul'] = "Tanyakost.com - Semua Kost"; // Data yang tampil di title page

    $this->load->view('admin/v_tampil_kost_detail', $data);
  }

  public function ubah_kost() {
    $id_admin = $this->session->userdata('c_id_ad'); // ambil id user dari session dengan key C_id (cookie id :D)

    $this->load->model('kost_m');
    $this->load->model('admin_m');

    // $kost = $this->kost_m->
    $data['judul'] = "Tanyakost.com | Ubah kost";
    $this->load->view('admin/v_ubah_kost', $data);
  }

}
