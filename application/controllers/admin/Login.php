<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

  public function __construct() {
    parent:: __construct();
    $this->load->helper('form');
    $this->load->library('form_validation');
    $this->load->model('Account_m');
    $this->load->model('Admin_m');
  }

  public function index()
  {
    $data['judul'] = "Tanyakost.com | Tempat cari kost";

    if(!$this->Account_m->validasi_cookie_admin()) { // mengecek apakah sudah terdapat cookie di browser
      $post = $this->input->post('login-admin'); // ambil nilai login-admin dari form disimpan ke var $post

      if($post){ // mengecek apakah ada nilai pada variabel $post

        $this->Account_m->set_email($this->input->post('admin-email')); // setter nilai ke variabel $email di model Account_m
        $this->Account_m->set_password($this->input->post('admin-password')); // setter nilai ke variabel $email di model Account_m

        // validasi form untuk email dan password
        $this->form_validation->set_rules('admin-email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules(
                                'admin-password', 'Password', 'required|min_length[4]');

        if ($this->form_validation->run() == FALSE) { // mengecek jika validasi bernilai false
           $data['error'] = '<strong>Peringatan!</strong> pastikan semua form telah diisi dengan benar.';
           // string yang akan ditampilkan di alert jika validasi gagal
        } else { // jika validasi berhasil
          if($this->Account_m->proses_login_admin()){ // mengecek dan menjalankan fungsi login yang ada di model account_m
            redirect('admin'); // jika berhasil di redirect ke controller user
          } else {
            $data['error'] = "Email dan password tidak sesuai, silahkan periksa kembali."; 
            // jika tidak berhasil akan menampilkan string error di alert
          }
        }
      } else {
        $data['error']   = $this->session->flashdata('error'); // ambil error ketika akses halaman yang perlu login
      }
    } else {
      redirect('admin');
    }

    $this->load->view('admin/v_login_admin', $data);
  }

  public function logout() {
    $this->Account_m->proses_logout();
    redirect(base_url());
  }
}
