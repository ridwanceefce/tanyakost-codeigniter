<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

  public function __construct() {
    parent:: __construct();

    $this->load->model('Account_m');

    if(!$this->Account_m->validasi_cookie_admin()) {
      $this->session->set_flashdata('notif', 'Akses ditolak');
      redirect('login-user');
    }
  }
  
  public function index() 
  { 
    $id_admin = $this->session->userdata('c_id_ad'); // ambil id user dari session dengan key C_id (cookie id :D)

    $this->load->model('admin_m'); // load model admin_m
    $this->load->model('user_m'); // load model user_m

    $admin = $this->admin_m->ambil_semua_data_admin($id_admin);
    $users = $this->user_m->ambil_user_terbaru()->result();

    $data['no']    = 1;
    $data['admin'] = $admin;
    $data['users'] = $users;
    $data['judul'] = "Tanyakost.com | Tempat cari kost";
    $this->load->view('admin/v_tampil_user', $data);
  }

  public function tampil_detail_user() { // mestinya ada id yang diterima dari view 
    $id_admin = $this->session->userdata('c_id'); // ambil id user dari session dengan key C_id (cookie id :D)
    $id_user  = $this->uri->segment(4);

    $this->load->model('admin_m'); // load model admin_m
    $this->load->model('user_m'); // load model user_m

    $user   = $this->user_m->ambil_user($id_user);
    $admin  = $this->admin_m->ambil_semua_data_admin($id_admin);

    $data['admin'] = $admin;
    $data['user']  = $user;
    $data['judul'] = "Tanyakost.com | Tempat cari kost";

    $this->load->view('admin/v_tampil_user_detail', $data);
  }

  public function ubah_user() {
    $id_admin = $this->session->userdata('c_id'); // ambil id user dari session dengan key C_id (cookie id :D)
    $id_user  = $this->uri->segment(4);

    $this->load->helper('form');

    $this->load->model('user_m');
    $this->load->model('admin_m');

    $user = $this->user_m->ambil_user($id_user);
    $admin = $this->admin_m->ambil_semua_data_admin($id_admin);

    $data['user']   = $user;
    $data['admin']  = $admin;
    
    $data['judul'] = "Tanyakost.com | Tempat cari kost";

    $this->load->view('admin/v_ubah_user', $data);
  }

  public function ubah_user_simpan() {
    $id_user = $this->input->post('id_user');
    $id_admin = $this->input->post('id_admin');

    $user = array(
      'nama'  => $this->input->post('user-name'),
      'email' => $this->input->post('email-user'),
      'provinsi' => $this->input->post('provinsi'),
      'kota_kabupaten' => $this->input->post('kota-kabupaten'),
      'kecamatan' => $this->input->post('kecamatan'),
      'jalan' => $this->input->post('jalan'),
      'telepon' => $this->input->post('telp-user'));
    
    $this->db->where('id_user', $id_user);

    $this->db->update('user', $user);

    redirect('admin/'.$id_admin.'/user/'.$id_user);
  }

  public function hapus_user(){
    $id_admin = $this->session->userdata('c_id'); // ambil id user dari session dengan key C_id (cookie id :D)
    $id_user = $this->uri->segment(4);

    $this->db->delete('user', array('id_user' => $id_user));

    redirect('admin/'.$id_admin.'/users');
  }
}
