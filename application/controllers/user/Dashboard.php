<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

  public function __construct() {
    parent::__construct();
    // Your own constructor code
    $this->load->helper('text'); // load helper text untuk limit karakter isi
    $this->load->helper('form'); //
    $this->load->model('user_m'); // load model user_m
    $this->load->model('pesan_m'); // load model pesan_m
    $this->load->model('kost_m'); // load model kost_m
    $this->load->model('Account_m');

    if(!$this->Account_m->validasi_cookie_user()) {
      $this->session->set_flashdata('notif', 'Silahkan login terlebih dahulu');
      redirect('login-user');
    }

  }

  public function index()
  {
    $id_user = $this->session->userdata('c_id_u'); // ambil id user dari session dengan key C_id (cookie id :D)

    $this->load->helper('text'); // untuk mengambil function dari helper text

    $user = $this->user_m->ambil_user($id_user); // simpan hasil return data dari function tampil_user ke variabel $user
    $pesan = $this->pesan_m->ambil_pesan_user($id_user);
    $kost = $this->kost_m->ambil_kost_user($id_user)->result();
    $total_kost = $this->kost_m->ambil_total_kost_user($id_user);
    $total_pesan = $this->pesan_m->ambil_total_pesan_user($id_user);

    $data['total_pesan'] = $total_pesan;
    $data['total_kost'] = $total_kost;
    $data['notif'] = $this->session->flashdata('notif');
    $data['user'] = $user; // masukkan data $user ke dalam array $data dengan key user
    $data['pesan'] = $pesan; // masukkan data $pesan ke dalam array $data dengan key pesan 
    $data['kost'] = $kost; // masukkan data $kost ke dalam array $data dengan key kost 
    $data['no'] = 1; // untuk penomoran pesan
    $data['judul'] = "Tanyakost.com | Tempat cari kost"; // variabel judul
    $this->load->view('user/v_dashboard_user', $data); // panggil view 
  }

  public function ubah_profil() {
    $id_user = $this->session->userdata('c_id_u'); // ambil id user dari session dengan key C_id (cookie id :D)

    $total_kost = $this->kost_m->ambil_total_kost_user($id_user);
    $total_pesan = $this->pesan_m->ambil_total_pesan_user($id_user);

    $data['total_pesan'] = $total_pesan;
    $data['total_kost'] = $total_kost;

    $user = $this->user_m->ambil_user($id_user); // ambil data user dengan id $user_id dengan tipe data array
    $data['user'] = $user;
    $data['judul'] = "Tanyakost.com | Tempat cari kost";
    $this->load->view('user/v_ubah_profil', $data);
  }

  public function ubah_profil_simpan() {
    $id_user = $this->input->post('id_user');

    $user = array(
      'nama'  => $this->input->post('user-name'),
      'email' => $this->input->post('email-user'),
      'provinsi' => $this->input->post('provinsi'),
      'kota_kabupaten' => $this->input->post('kota-kabupaten'),
      'kecamatan' => $this->input->post('kecamatan'),
      'jalan' => $this->input->post('jalan'),
      'telepon' => $this->input->post('telp-user'));

    if(!empty($_FILES['foto-profil']['tmp_name'])) { // mengecek apakah terdapat file pada tag input type files
      // $id_terakhir = $this->user_m->ambil_id_terakhir()->id_user;
      // $id_baru = ++$id_terakhir; 
      $nama_array = explode('.', basename($_FILES['foto-profil']['name'])); // memecah nama file 
      // miral.jpg menjadi array('miral'. 'jpg');
      $jml_arr = count($nama_array); // menghitung jumlah element dalam array

      $nama_file_asli = strtolower("pemilik-kost".'-'.$id_user.'.'.$nama_array[$jml_arr-1]);
      // membuat nama baru dengan format 

      $config['upload_path'] = './assets/images/pemilik/';
      $config['allowed_types'] = 'jpg|jpeg|png';
      $config['file_name'] = $nama_file_asli;
      $config['overwrite'] = true;

      $this->load->library('Upload', $config); // memanggil library upload dengan konfigurasi seperti yang
      // di atas

      $img = "foto-profil"; // name dari tag input tempat file foto di upload

      if(!$this->upload->do_upload($img)) { // melakukan function move_uploaded_file(filename, destination)
        $user['foto'] = 'default2.png'; // jika upload tidak berhasil maka nama di database akan menjadi 'default.png'
      } else {
        $user['foto'] = $this->upload->data('file_name'); // jika proses upload berhasil maka nama file di 
        // repository akan menjadi pemilik-kost-12.jpg - 12 adalah id user
      }
    } else {
      $user['foto'] = 'default1.png'; // jika tidak terdapat file di tag input foto-profil makan 
      // nama di database akan menjadi 'default.png'
    }

    $this->db->where('id_user', $id_user);

    $this->db->update('user', $user);

    $this->session->set_flashdata('notif', 'Berhasil ubah data profil');
    redirect('user');
  }
}