<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kost extends CI_Controller {

  public function __construct() {
    parent::__construct();
    // Your own constructor code
    $this->load->helper('text'); // load helper text untuk limit karakter isi
    $this->load->helper('form');

    $this->load->model('user_m'); // load model user_m
    $this->load->model('kost_m'); // load model kost_m
    $this->load->model('pesan_m');
    $this->load->model('Account_m');

    if(!$this->Account_m->validasi_cookie_user()) {
      $this->session->set_flashdata('error', 'Silahkan login terlebih dahulu');
      redirect('login-user');
    }    
  }

  public function index() 
  {
    $id_user = $this->session->userdata('c_id_u'); // ambil id user dari session dengan key C_id (cookie id :D)

    $user = $this->user_m->ambil_user($id_user);
    $kost = $this->kost_m->ambil_kost_user($id_user)->result();

    $total_kost = $this->kost_m->ambil_total_kost_user($id_user);
    $total_pesan = $this->pesan_m->ambil_total_pesan_user($id_user);

    $data['total_pesan'] = $total_pesan;
    $data['total_kost'] = $total_kost;

    $data['user'] = $user;
    $data['kost'] = $kost;
    $data['notif'] = $this->session->flashdata('notif');
    $data['judul'] = "Tanyakost.com | Tempat cari kost";
    
    $this->load->view('user/v_tampil_kost', $data);
  }

  public function tampil_kost_detail() {
    $id_user = $this->session->userdata('c_id_u'); // ambil id user dari session dengan key C_id (cookie id :D)
    $id_kost = $this->uri->segment(2);

    $user = $this->user_m->ambil_user($id_user);
    $kost = $this->kost_m->ambil_kost_detail($id_kost)->row_array();
    $total_kost = $this->kost_m->ambil_total_kost_user($id_user);
    $total_pesan = $this->pesan_m->ambil_total_pesan_user($id_user);

    $data['total_pesan'] = $total_pesan;
    $data['total_kost'] = $total_kost;

    $fasilitas = explode(', ', $kost['fasilitas']); // pecah string fasilitas menjadi array

    $data['fasilitas']  = $fasilitas;
    $data['user']       = $user;
    $data['kost']       = $kost;
    $data['notif']      = $this->session->flashdata('notif');
    $data['judul']      = "Tanyakost.com | Tempat cari kost";

    $this->load->view('user/v_detail_kost', $data);
  }

  public function input_kost() {
    $id_user = $this->session->userdata('c_id_u'); // ambil id user dari session dengan key C_id (cookie id :D)

    $total_kost = $this->kost_m->ambil_total_kost_user($id_user);
    $total_pesan = $this->pesan_m->ambil_total_pesan_user($id_user);

    $data['total_pesan'] = $total_pesan;
    $data['total_kost'] = $total_kost;

    $user = $this->user_m->ambil_user($id_user);

    $data['user'] = $user;
    $data['judul'] = "Tanyakost.com";
    
    $this->load->view('user/v_tambah_kost', $data);
  }

  public function ubah_kost() { // mestinya ada id yang di terima dari view
    $id_user = $this->session->userdata('c_id_u'); // ambil id user dari session dengan key C_id (cookie id :D)
    $id_kost = $this->uri->segment(2);

    $array = array(
      'id_kost' => $id_kost,
      'id_user' => $id_user );

    $kost = $this->kost_m->ambil_kost_id_user($array)->row_array();
    $user = $this->user_m->ambil_user($id_user);
    $total_kost = $this->kost_m->ambil_total_kost_user($id_user);
    $total_pesan = $this->pesan_m->ambil_total_pesan_user($id_user);

    $data['total_pesan'] = $total_pesan;
    $data['total_kost'] = $total_kost;
    $data['kost']   = $kost;
    $data['user']   = $user;
    $data['judul']  = "Tanyakost.com";

    $this->load->view('user/v_ubah_kost', $data);
  }

  public function ubah_kost_simpan() {
    $id_kost = $this->input->post('id_kost');
    $id_user = $this->input->post('id_user');

    $kost = array(
      'nama_kost'   => $this->input->post('nama-kost'),
      'jenis_kost'  => $this->input->post('jenis-kost'),
      'tipe_kost'   => $this->input->post('tipe-kost'),
      'harga'       => $this->input->post('harga'),
      'provinsi'    => $this->input->post('provinsi'),
      'kecamatan'   => $this->input->post('kecamatan'),
      'kota_kabupaten' => $this->input->post('kota-kabupaten'),
      'jalan'       => $this->input->post('jalan-user'),
      'deskripsi'   => $this->input->post('deskripsi-kost'),
      'fasilitas'   => $this->input->post('fasilitas') );

    if(!empty($_FILES['foto-kost']['tmp_name'])){
      $nama_array = explode('.', $_FILES['foto-kost']['name']);
      $pjg_arr = count($nama_array);

      $nama_kost_asli = strtolower('kost-'.$id_kost.'-pemilik-'.$id_user.'.'.$nama_array[$pjg_arr-1]);

      $config['upload_path'] = './assets/images/kosts/';
      $config['allowed_types'] = 'jpg|jpeg|png';
      $config['file_name'] = $nama_kost_asli;
      $config['overwrite'] = true;

      $this->load->library('Upload', $config); 

      $img = 'foto-kost';
      if(!$this->upload->do_upload($img)) {
        // $this->session->set_flashdata('notif', $this->upload->display_errors());
        $kost['foto_kost'] = 'default.png';
      } else {
        // $this->session->set_flashdata('notif', $this->upload->display_errors());
        $kost['foto_kost'] = $this->upload->data('file_name');
      } 
    } else {
      // $this->session->set_flashdata('notif', $this->upload->display_errors());
      $kost['foto_kost'] = 'default.png';
    }

    $this->db->where('id_kost', $id_kost);
    $this->db->where('id_user', $id_user);
    $this->db->update('kost', $kost);

    $this->session->set_flashdata('notif', 'Berhasil ubah data kost');

    redirect('kost-user/'.$id_kost);
  }

  public function input_kost_simpan() {
    $id_user = $this->input->post('id_user');

    $kost = array(
      'id_user'     => $id_user,
      'nama_kost'   => $this->input->post('nama-kost'),
      'jenis_kost'  => $this->input->post('jenis-kost'),
      'tipe_kost'   => $this->input->post('tipe-kost'),
      'harga'       => $this->input->post('harga'),
      'provinsi'    => $this->input->post('provinsi'),
      'kota_kabupaten' => $this->input->post('kota-kabupaten'),
      'kecamatan'   => $this->input->post('kecamatan'),
      'jalan'       => $this->input->post('jalan'),
      'deskripsi'   => $this->input->post('deskripsi'),
      'fasilitas'   => $this->input->post('fasilitas'));

    if(!empty($_FILES['foto-kost']['tmp_name'])) {
      $nama_array = explode('.', $_FILES['foto-kost']['name']);
      $pjg_arr = count($nama_array);

      $id_terakhir = $this->kost_m->ambil_id_terakhir()->id_kost; // mengambil kolom id_kost
      $id_baru = ++$id_terakhir; 

      $nama_kost_asli = strtolower('kost-'.$id_baru.'-pemilik-'.$id_user.'.'.$nama_array[$pjg_arr-1]);

      $config['upload_path'] = './assets/images/kosts/';
      $config['allowed_types'] = 'jpg|jpeg|png';
      $config['file_name'] = $nama_kost_asli;
      $config['overwrite'] = true;

      $this->load->library('Upload', $config);

      $img = 'foto-kost';
      if(!$this->upload->do_upload($img)) {
        // $this->session->set_flashdata('notif', $this->upload->display_errors());
        $kost['foto_kost'] = 'default2.png';
      } else {
        // $this->session->set_flashdata('notif', $this->upload->display_errors());
        $kost['foto_kost'] = $this->upload->data('file_name');
      } 
    } else {
      $kost['foto_kost'] = 'default1.png';
    }

    $this->db->insert('kost', $kost);
    $this->session->set_flashdata('notif', 'Berhasil tambah data kost');
    redirect('kost-user');
  }

  public function hapus_kost() {
    $id_user = $this->session->userdata('c_id_u'); // ambil id user dari session dengan key C_id (cookie id :D)
    $id_kost = $this->uri->segment(2);

    $this->db->delete('kost', array('id_kost' => $id_kost, 'id_user' => $id_user));
    $this->session->set_flashdata('notif', 'Berhasil hapus data kost');
    redirect('kost-user');
  }
}
