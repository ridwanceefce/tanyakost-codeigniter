<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pesan extends CI_Controller {

  public function __construct() {
    parent::__construct();
    // Your own constructor code
    $this->load->helper('text'); // load helper text untuk limit karakter isi
    $this->load->model('user_m'); // load model user_m
    $this->load->model('pesan_m'); // load model pesan_m
    $this->load->model('kost_m');
    $this->load->model('Account_m');

    if(!$this->Account_m->validasi_cookie_user()) {
      $this->session->set_flashdata('error', 'Silahkan login terlebih dahulu');
      redirect('login-user');
    } 
  }

  public function index() {

    $id_user = $this->session->userdata('c_id_u'); // ambil id user dari session dengan key C_id (cookie id :D)

    $user = $this->user_m->ambil_user($id_user);
    $pesan = $this->pesan_m->ambil_pesan_user($id_user); // menyimpan data yang di ambil dari function ambil_semua_pesan() dan di konversi ke object

    $total_kost = $this->kost_m->ambil_total_kost_user($id_user);
    $total_pesan = $this->pesan_m->ambil_total_pesan_user($id_user);

    $data['total_pesan'] = $total_pesan;
    $data['total_kost'] = $total_kost;

    $no = 1;
    $data['no'] = $no;
    $data['pesan'] = $pesan; // masukkan data $pesan ke dalam array data untuk view
    $data['user'] = $user;
    $data['notif'] = $this->session->flashdata('notif');
    $data['judul'] = "Tanyakost.com | Tempat cari kost";
    $this->load->view('user/v_tampil_pesan', $data);
  }

  public function tambah_pesan($id_kost) {
    $id_penerima   = $this->input->post('id');
    $isi_pesan  = $this->input->post('pesan');
    $email      = $this->input->post('email');

    $total_kost = $this->kost_m->ambil_total_kost_user($id_user);
    $total_pesan = $this->pesan_m->ambil_total_pesan_user($id_user);

    $data['total_pesan'] = $total_pesan;
    $data['total_kost'] = $total_kost;

    $data_pesan = array(
      'id_user'   => $id_penerima,
      'pengirim'  => $email,
      'isi'       => $isi_pesan);

    $this->db->insert('pesan', $data_pesan);
    $this->session->set_flashdata('notif', 'Berhasil mengirim pesan');
    redirect('detail/'.$id_kost);
  }

  public function baca_pesan() { // mestinya harus ada id pesan untuk query select ke database
    $id_user = $this->session->userdata('c_id_u'); // ambil id user dari session dengan key C_id (cookie id :D)
    $id_pesan = $this->uri->segment(3);

    $total_kost = $this->kost_m->ambil_total_kost_user($id_user);
    $total_pesan = $this->pesan_m->ambil_total_pesan_user($id_user);

    $data['total_pesan'] = $total_pesan;
    $data['total_kost'] = $total_kost;


    $pesan = $this->pesan_m->baca_pesan($id_pesan)->row_array();
    $user = $this->user_m->ambil_user($id_user);

    $data['pesan'] = $pesan;
    $data['user'] = $user;
    $data['notif'] = '';
    $data['judul'] = "Tanyakost.com | Tempat cari kost";
    $this->load->view('user/v_baca_pesan', $data);
  }

  public function hapus_pesan() {
    $id_user  = $this->session->userdata('c_id_u'); // ambil id user dari session dengan key C_id (cookie id :D)
    $id_pesan = $this->uri->segment(3);

    $this->db->delete('pesan', array('id' => $id_pesan));
    $this->session->set_flashdata('notif', 'Pesan berhasil di hapus');
    redirect('pesan-user');
  }

}
