<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registrasi extends CI_Controller {
  
  public function __construct() {
    parent:: __construct();
    $this->load->helper('form');
    $this->load->library('form_validation');
    $this->load->model('Account_m');
  }

  public function index() {
    $data['judul'] = "Tanyakost.com | Tempat cari kost";

    
      // konfigurasi validasi form
      $this->form_validation->set_rules('user-name', 'Username', 'required');
      $this->form_validation->set_rules('user-email', 'Email', 'required|valid_email');
      $this->form_validation->set_rules(
        'user-password', 'Password', 'required|min_length[4]|matches[user-password-conf]');
      $this->form_validation->set_rules(
        'user-password-conf', 'Konfirmasi Password', 'required|min_length[4]');

      if ($this->form_validation->run() == FALSE) {
        $data['error'] =  validation_errors();
        $data['notif'] = '';
        $this->load->view('user/v_registrasi_user', $data);
      } else {
        $this->registrasi_simpan();
      }
  }

  public function registrasi_simpan() {
    $user = array(
      'email' => $this->input->post('user-email'),
      'password' => md5($this->input->post('user-password')),
      'nama' => $this->input->post('user-name') );

    if($this->Account_m->proses_registrasi_login($user)){
      $id_user = $this->session->userdata('c_id_u'); // ambil data id dari session
      $this->session->set_flashdata('notif', 'Silahkan lengkapi data akun anda dengan mengklik ubah profil');
      redirect('user');
    }
  }
}
