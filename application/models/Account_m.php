<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account_m extends CI_Model {

  var $email;
  var $pass;
  var $user_data;

  public function __construct(){
    $this->email = '';
    $this->pass = '';
  }

  public function set_email($email) {
    $this->email = $email;
    $this->user_data['c_email'] = $email;
  }

  public function set_password($pass) {
    $this->pass = $pass;
    $this->user_data['c_pass'] = md5($pass);
  }

  /**
  * parameter $array : tipe array
  * return boolean (true atau false)
  */
  public function proses_registrasi_login($user) {

    $this->db->insert('user', $user); // query insert ke tabel user di database,
    $last_id = $this->db->insert_id(); // ambil id yang terakhir dimasukkan ke database
    $baris = $this->ambil_user($last_id); // panggil function ambil user di model account_m dengan parameter last_id

    if ($this->db->affected_rows() > 0) { // jika jumlah baris yang terkena pengaruh query lebih dari 0 maka
      $this->user_data['c_id_u'] = $baris['id_user']; // tambahkan data id ke array user_data
      $this->user_data['c_email_u'] = $baris['email']; // tambahkan data email ke array user_data
      $this->user_data['c_pass_u'] = $baris['password']; // tambahkan data password ke array data

      $this->set_cookie(); // memanggil fungsi set_cookie di model account_m

      return true; // jika berhasil return true
    } else {
      return false; // jika tidak berhasil return false
    }
  }

  public function proses_login() {
    $this->db->select('*');
    $this->db->from('user');
    $this->db->where('email', $this->email);
    $this->db->where('password', md5($this->pass));
    $baris = $this->db->get()->row_array();

    if ($this->db->affected_rows() > 0) {
      $this->user_data['c_id_u'] = $baris['id_user']; // tambahkan data id ke array user_data
      $this->user_data['c_email_u'] = $baris['email']; // tambahkan data email ke array user_data
      $this->user_data['c_pass_u'] = $baris['password']; // tambahkan data password ke array data

      $this->set_cookie();

      return true;
    } else {
      return false;
    }
  }

  /**
  * nama: proses_login_admin
  * parameter: tidak ada
  * return : boolean - true jika berhasil login, false jika gagal login
  * deskripsi: untuk menyimpan cookie dengan data dari array user_data pada model Account_m
  */
  public function proses_login_admin(){
    $this->db->select('*');
    $this->db->from('admin');
    $this->db->where('email', $this->email);
    $this->db->where('password', md5($this->pass));
    $baris = $this->db->get()->row_array();

    if ($this->db->affected_rows() > 0) {
      $this->user_data['c_id_ad'] = $baris['id']; // tambahkan data id ke array user_data
      $this->user_data['c_email_ad'] = $baris['email']; // tambahkan data email ke array user_data
      $this->user_data['c_pass_ad'] = $baris['password']; // tambahkan data password ke array data

      $this->set_cookie();

      return true;
    } else {
      return false;
    }
  }

  /**
  * nama: set_cookie
  * parameter: tidak ada
  * return : tidak ada
  * deskripsi: untuk menyimpan cookie dengan data dari array user_data pada model Account_m
  */
  protected function set_cookie(){ // menyimpan data ke cookie
    $this->session->set_userdata($this->user_data); // array user_data
  }

  /**
  * nama: validasi_cookie
  * parameter: tidak ada
  * return : boolean true / false - jika session user atau admin ada maka true, jika tidak maka false
  * deskripsi : untuk melakukan pengecekkan apakah sudah ada cookie di browser
  */
  public function validasi_cookie_user() {
    if($this->session->userdata('c_id_u') != '' && $this->session->userdata('c_pass_u') != '') {
      return true;
    } else {
      return false;
    }
  }

  public function validasi_cookie_admin() {
    if($this->session->userdata('c_id_ad') != '' && $this->session->userdata('c_pass_ad') != '')  {
      return true;
    } else {
      return false;
    }
  }


  /**
  * nama: ambil_user
  * parameter: $id - tipe: integer
  * return : array data dari database
  * deskripsi: untuk mengambil data dari database dengan syarat id_user = $id
  */
  protected function ambil_user($id) {
    $this->db->select('*');
    $this->db->from('user');
    $this->db->where('id_user', $id);
    $hasil = $this->db->get();
    return $hasil->row_array();
  }

  /**
  * nama: ambil_user
  * parameter: $id - tipe: integer
  * return : array data dari database
  * deskripsi: untuk mengambil data dari database dengan syarat id_user = $id
  */
  public function proses_logout() {
    $sess = array(
      'c_id', '',
      'c_email', '',
      'c_pass', '');

    $this->session->unset_userdata($sess);

    session_destroy();
    $this->session->sess_destroy();
  }

}

/* End of file Account_m.php */
/* Location: ./application/models/Account_m.php */