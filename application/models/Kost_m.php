<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kost_m extends CI_Model {

  public function ambil_semua_kost() {
    $kost = $this->db->get('kost');
    return $kost;
  }

  public function ambil_kost_terbaru() {
    $this->db->select('*');
    $this->db->from('kost');
    $this->db->order_by('id_kost', 'desc');
    $hasil = $this->db->get();
    return $hasil;
  }
 
  public function ambil_kost_user($id_user) {
    $this->db->select('*');
    $this->db->from('kost');
    $this->db->where('id_user', $id_user);
    $hasil = $this->db->get();
    return $hasil;
  }

  public function ambil_kost_detail($id_kost) {
    $this->db->select('*');
    $this->db->from('kost');
    $this->db->where('id_kost', $id_kost);
    $hasil = $this->db->get();
    return $hasil;
  }

  public function ambil_kost_id($id_kost) {
    $this->db->select('*');
    $this->db->from('kost');
    $this->db->where('id_kost', $id_kost);
    $hasil = $this->db->get();
    return $hasil;
  }

  public function ambil_kost_id_user($array) {
    $this->db->select('*');
    $this->db->from('kost');
    $this->db->where($array);
    $hasil = $this->db->get();
    return $hasil;
  }

  public function ambil_data_cari_kost($keyword) {
    $this->db->select('*');
    $this->db->from('kost');
    $this->db->join('user', 'kost.id_user = user.id_user');
    $this->db->like('kost.nama_kost', $keyword);
    $this->db->or_like('user.nama', $keyword);
    $this->db->or_like('kost.provinsi', $keyword);
    $this->db->or_like('kost.kecamatan', $keyword);
    $this->db->or_like('kost.kota_kabupaten', $keyword);
    $this->db->or_like('kost.jalan', $keyword);
    $this->db->or_like('kost.jenis_kost', $keyword);
    $this->db->or_like('kost.tipe_kost', $keyword);
    $this->db->or_like('kost.harga', $keyword);
    $hasil = $this->db->get();
    return $hasil;
  }

  public function ambil_total_kost_user($id_user) {
    $this->db->select('*');
    $this->db->from('kost');
    $this->db->where('id_user', $id_user);
    $hasil = $this->db->count_all_results();
    return $hasil;
  }


  public function ambil_id_terakhir() {
    $this->db->select('id_kost');
    $this->db->from('kost');
    $this->db->order_by('id_kost', 'desc');
    $hasil = $this->db->get();
    return $hasil->row();
  }
}