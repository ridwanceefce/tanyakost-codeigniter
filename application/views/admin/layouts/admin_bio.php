              <div class="col-md-12" id="profil-bio">
                <div class="bio-head">
                  <img src="<?= base_url('assets/images/admin/photo_400x400.jpg') ?>" class="img-responsive img-circle" alt="">
                  <div class="user-name-status">
                    <p class="user-name"><?= $admin['nama'] ?></p>
                    <p class="user-status">Admin</p>
                  </div>
                </div>
                <div class="clearfix"></div>
                <hr class="hr-primary">
                <div class="bio-control">
                  <ul class="control">
                    <li class="active">
                      <a href="<?= base_url('admin') ?>"><i class="fa fa-tachometer" aria-hidden="true"></i> Dashboard</a>
                    </li>
                    <li>
                      <a href="<?= base_url('admin/user') ?>"><span><i class="fa fa-user" aria-hidden="true"></i>  Users</span><span class="badge pull-right">1</span></a>
                    </li>
                    <li>
                      <a href="<?= base_url('admin/kost-user') ?>"><span><i class="fa fa-home" aria-hidden="true"></i> Kosts</span><span class="badge pull-right">1</span></a>
                    </li>
                    <li>
                      <a href="<?= base_url('admin/pesan-user') ?>"><span><i class="fa fa-envelope" aria-hidden="true"></i> Pesan</span><span class="badge pull-right">10</span></a>
                    </li>
                  </ul>
                </div> <!-- /.bio-control -->
                <hr class="hr-primary">
                <div class="settings">
                  <h3>Pengaturan</h3>
                  <ul class="profil-setting">
                    <li><a href="<?= base_url('admin/ubah-profil') ?>"><span><i class="fa fa-cogs" aria-hidden="true"></i>Ubah profil</span></a></li>
                    <li><a href="<?= base_url('logout-admin') ?>"><span><i class="fa fa-sign-out" aria-hidden="true"></i>Log out</span></a></li>
                  </ul>
                </div>
              </div> <!-- /#profil-bio -->