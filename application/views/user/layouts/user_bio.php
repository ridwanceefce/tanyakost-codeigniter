              <div class="col-md-12" id="profil-bio">
                <div class="bio-head">
                  <img src="<?= base_url('assets/images/pemilik/'.$user['foto']) ?>" class="img-responsive img-circle" alt="">
                  <div class="user-name-status">
                    <p class="user-name"><?= $user['nama'] ?></p>
                    <p class="user-status">Pemilik Kost</p>
                  </div>
                </div>
                <div class="clearfix"></div>
                <hr class="hr-primary">
                <div class="bio-control">
                  <!-- <button class="btn btn-tambah">tambah kost</button> -->
                  <a href="<?= base_url('tambah-kost') ?>" class="btn btn-tambah">tambah kost</a>
                  <ul class="control">
                    <li><a href="<?= base_url('user') ?>"><span><i class="fa fa-tachometer" aria-hidden="true"></i> Dashboard</span></a></li>
                    <li><a href="<?= base_url('kost-user') ?>"><span><i class="fa fa-home" aria-hidden="true"></i> Kosts</span><span class="badge pull-right"><?= $total_kost ?></span></a></li>
                    <li><a href="<?= base_url('pesan-user') ?>"><span><i class="fa fa-envelope" aria-hidden="true"></i> Pesan</span><span class="badge pull-right"><?= $total_pesan ?></span></a></li>
                  </ul>
                </div> <!-- /.bio-control -->
                <hr class="hr-primary">
                <div class="settings">
                  <h3>Pengaturan</h3>
                  <ul class="profil-setting">
                    <li><a href="<?= base_url('ubah-profil') ?>"><i class="fa fa-user" aria-hidden="true"></i>Ubah profil</a></li>
                    <li><a href="<?= base_url('logout-user') ?>"><span><i class="fa fa-sign-out" aria-hidden="true"></i>Log out</span></a></li>
                  </ul>
                </div>
              </div>