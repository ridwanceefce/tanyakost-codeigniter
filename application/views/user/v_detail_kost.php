<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <?php $this->load->view('layouts/favico') ?>
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="images/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <title><?php echo $judul ?></title>

    <!-- Bootstrap Core CSS -->
    <link href="<?= base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?= base_url('assets/css/user-detail-kost.css') ?>" rel="stylesheet">
    <link rel="stylesheet" href="<?= base_url('assets/css/user-bio.css') ?>" >
    <link rel="stylesheet" href="<?= base_url('assets/css/navbar-main.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/normalize.css') ?>">

    <!-- Custom Fonts -->
    <link href="<?= base_url('assets/font-awesome/css/font-awesome.min.css') ?>" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,500,700,300' rel='stylesheet' type='text/css'>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!-- jQuery -->
    <script src="<?= base_url('assets/js/jquery.js') ?>"></script>

    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>

    <header class="container-fluid">
      <?php $this->load->view('layouts/navbar-main') ?>
    </header>

      <div class="container-fluid">
        <?php if($notif != ''): ?>
          <div class="row">
            <div class="col-md-12 pd-rl-10">
              <div id="notif" class="alert alert-success alert-dismissible" data-dismiss="alert" role="alert">
                <strong>Success!</strong> <?= $notif ?>.
              </div>
            </div>
          </div>
        <?php endif; ?>

        <div class="row">
          <div class="" id="main-profil">
            <div class="col-md-2 col-sm-4 pd-rl-5" >
              <?php $this->load->view('user/layouts/user_bio') ?>
            </div>

            <div class="col-md-10 col-sm-8 pd-rl-5" >
              <div class="col-md-12" id="profil-konten">
                <div id="tampil-kost" class="col-md-12">
                  <a href="<?= base_url('kost-user') ?>" class="btn btn-danger mg-tb-10">TAMPILKAN SEMUA KOST</a>
                  <div class="kost-item">
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <h3>Kost #<?= $kost['id_kost'] ?></h3>
                      </div>
                      <div class="panel-body">
                        <div id="kost-bio" class="col-md-8">
                          <div id="kost-name">
                            <h5>Nama Kost</h5>
                            <div class="well">
                              <p><?= $kost['nama_kost'] ?></p>
                            </div>
                          </div>
                          <div id="kost-tipe">
                            <h5>Tipe Kost</h5>
                            <div class="well">
                              <p><?= $kost['tipe_kost'] ?></p>
                            </div>
                          </div>
                          <div id="kost-jenis">
                            <h5>Jenis Kost</h5>
                            <div class="well">
                              <p><?= $kost['jenis_kost'] ?></p>
                            </div>
                          </div>
                          <div id="kost-harga">
                            <h5>Harga</h5>
                            <div class="well">
                              <p>Rp. <?= $kost['harga'] ?></p>
                            </div>
                          </div>
                          <div id="kost-alamat">
                            <h5>Alamat Lengkap</h5>
                            <div class="well">
                              <p>
                                <?= $kost['jalan'] ?>, 
                                <?= $kost['kecamatan'] ?>, 
                                <?= $kost['kota_kabupaten'] ?>, 
                                <?= $kost['provinsi'] ?>
                              </p>
                            </div>
                          </div>
                          <div id="kost-fasilitas">
                            <h5>Fasilitas Kost</h5>
                            <div class="well">
                              <ul>
                                <?php foreach($fasilitas as $f): ?>
                                  <li><?= $f ?></li>
                                <?php endforeach; ?>
                              </ul>
                            </div>
                          </div>
                        </div>
                        <div id="user-foto" class="col-md-4">
                          <div class="thumbnail">
                            <img src="<?= base_url('assets/images/kosts/'.$kost['foto_kost']) ?>" alt="">
                          </div>
                        </div>
                      </div>
                      <div class="panel-footer">
                        <a href="<?= base_url('ubah-kost/'.$kost['id_kost']) ?>" class="btn btn-success">Perbaharui Kost</a>
                        <a href="<?= base_url('hapus-kost/'.$kost['id_kost']) ?>" class="btn btn-danger">Hapus Kost</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>  <!-- /#profil-konten -->
            </div>
          </div> <!-- /#main-profil -->
        </div>
      </div> <!-- /.container-fluid -->

      <!-- footer -->
      <div class="container-fluid">
        <?php $this->load->view('layouts/footer-main') ?>
      </div>
      <!-- /footer -->
  </body>
</html>
