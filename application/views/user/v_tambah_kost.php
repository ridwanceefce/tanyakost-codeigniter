<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <?php $this->load->view('layouts/favico') ?>
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">

    <title><?php echo $judul ?></title>

    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="<?= base_url('assets/css/bootstrap.min.css') ?>">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="<?= base_url('assets/css/user-kost-input.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/navbar-main.css') ?>"> 
    <link rel="stylesheet" href="<?= base_url('assets/css/user-bio.css') ?>"> 
    <link rel="stylesheet" href="<?= base_url('assets/css/normalize.css') ?>">

    <!-- Custom Fonts -->
    <link href="<?= base_url('assets/font-awesome/css/font-awesome.min.css') ?>" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,500,700,300' rel='stylesheet' type='text/css'>
    
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!-- jQuery -->
    <script src="<?= base_url('assets/js/jquery.js') ?>"></script>

    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>

    <header class="container-fluid">
      <?php $this->load->view('layouts/navbar-main') ?>
    </header>

    <div class="container-fluid">
      <div class="row">
        <div class="" id="main-profil">
          <div class="col-md-2 col-sm-4 pd-rl-5" >
            <?php $this->load->view('user/layouts/user_bio'); ?>
          </div>
          <div class="col-md-10 col-sm-8 pd-rl-5" >
            <div class="col-md-12" id="profil-konten">
              <h3><i class="fa fa-list-alt" aria-hidden="true"></i> Form tambah kost</h3>
              <hr class="hr-primary">
              <?= form_open_multipart('user/kost/input_kost_simpan'); ?>
                <?= form_hidden('id_user', $user['id_user']); ?>
                <div class="row">
                  <div class="col-md-8">
                    <div id="user-pribadi" class="panel panel-default">
                      <div class="panel-heading">
                        <h4>Data Kost</h4>
                      </div>
                      <div class="panel-body">
                        <label>Nama Kost</label>
                        <input type="text" name="nama-kost" id="nama-kost" class="form-control">
                        <span id="help-nama-user" class="help-block mg-b-1em"><i class="fa fa-question-circle" aria-hidden="true"></i> nama lengkap akan ditampilkan pada detail iklan kost.</span>

                        <label>Jenis Kost</label>
                        <input type="text" name="jenis-kost" id="jenis-kost" class="form-control" placeholder="inputkan jenis kost...">
                        <span id="help-nama-user" class="help-block mg-b-1em"><i class="fa fa-question-circle" aria-hidden="true"></i> Pilihan jenis kost : Putra, Putri, Pasutri</span>

                        <label>Tipe Kost</label>
                        <input type="text" name="tipe-kost" id="tipe-kost" class="form-control">
                        <span id="help-nama-user" class="help-block mg-b-1em"><i class="fa fa-question-circle" aria-hidden="true"></i> Pilihan tipe kost : Harian, Mingguan, Bulanan, Tahunan.</span>

                        <label>Harga</label>
                        <div class="input-group">
                          <span class="input-group-addon">Rp.</span>
                          <input type="text" name="harga" id="harga-kost" class="form-control mg-b-1em">
                        </div>
                      </div>
                    </div> <!-- /#user-pribadi -->

                    <div id="user-alamat" class="panel panel-default">
                      <div class="panel-heading">
                        <h4>Alamat kost</h4>
                      </div>
                      <div class="panel-body">
                        <label>Provinsi</label>
                        <input type="text" name="provinsi" id="provinsi" class="form-control" placeholder="inputkan nama provinsi">
                        <span id="help-nama-user" class="help-block mg-b-1em"><i class="fa fa-question-circle" aria-hidden="true"></i> contoh: D.I. Yogyakarta</span>

                        <label>Kabupaten / Kota</label>
                        <input type="text" name="kota-kabupaten" id="kota" class="form-control" placeholder="inputkan nama kota / kabupaten">
                        <span id="help-nama-user" class="help-block mg-b-1em"><i class="fa fa-question-circle" aria-hidden="true"></i> contoh: sleman</span>

                        <label>Kecamatan</label>
                        <input type="text" name="kecamatan" id="kecamatan" class="form-control" placeholder="inputkan nama kecamatan">
                        <span id="help-nama-user" class="help-block mg-b-1em"><i class="fa fa-question-circle" aria-hidden="true"></i> contoh: condongcatur</span>
                        
                        <label>Jalan</label>
                        <input type="text" name="jalan" class="form-control" placeholder="nama jalan lengkap">
                        <span class="help-block"><i class="fa fa-question-circle" aria-hidden="true"></i> contoh: Jl. Nusa Indah, No. 4, Condongcatur</span>
                      </div> 
                    </div> <!-- /#user-alamat -->
                    <div id="kost-deskripsi" class="panel panel-default">
                      <div class="panel-heading">
                        <h4>Deskripsi Kost</h4>
                      </div>
                      <div class="panel-body">
                       <textarea name="deskripsi" id="deskripsi-kost" rows="10" class="form-control"></textarea>
                      <span class="help-block">
                        <i class="fa fa-question-circle" aria-hidden="true"></i> 
                        Isikan deskripsi lengkap kost (contoh: peraturan, kondisi lingkungan sekitar kost)
                      </span>
                      </div>
                    </div>
                    <div id="user-contact" class="panel panel-default">
                      <div class="panel-heading">
                        <h4>Fasilitas kost</h4>
                      </div>
                      <div class="panel-body">
                        <textarea name="fasilitas" id="fasilitas" class="form-control"
                         placeholder="contoh: kamar mandi dalam, lemari, tempat tidur"></textarea>
                        <span class="help-block">
                          <i class="fa fa-question-circle" aria-hidden="true"></i> 
                          Dipisahkan dengan koma ( , )
                        </span>
                      </div>
                    </div> <!-- /#user-contact -->
                  </div>
                  <div class="col-md-4">
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <h4>Upload Foto Kost</h4>
                      </div>
                      <div class="panel-body  pd-rl-10">
                        <div class="thumbnail">
                          <img id="preview" src="<?= base_url('assets/images/kosts/default.png') ?>" alt="">
                        </div>
                          <input id="uploadBtn" type="file" name="foto-kost" class="upload">
                      </div>
                    </div>
                  </div> <!-- /.col-md-4 upload foto -->
                </div>
                <div class="row">
                  <div class="col-md-8">
                  <div id="action" class="panel panel-default">
                    <div class="panel-heading">
                      <h4>Action</h4>
                    </div>
                    <div class="panel-body">
                      <button type="submit" class="btn btn-success">Tambah kost</button>
                      <button type="reset" class="btn btn-danger">Batal</button>
                    </div>
                  </div><!--  /#action -->
                  <!-- <div class="clearfix"></div> -->
                  </div>
                </div>
                <?= form_close(); ?>
              </form>
            </div>  <!-- /#profil-konten -->
          </div>
        </div>
      </div>
    </div>

    <!-- footer -->
    <div class="container-fluid">
      <?php $this->load->view('layouts/footer-main'); ?>
    </div>
    <!-- /footer -->

    <!-- javascript -->
    <script type="text/javascript">
      // document.getElementById("uploadBtn").onchange = function () {
      //   document.getElementById("uploadFile").value = this.value;
      // };
      
      function readURL(input) {
        if (input.files && input.files[0]) {
          var reader = new FileReader();
          reader.onload = function (e) {
            $('#preview').attr('src', e.target.result);
          }
          reader.readAsDataURL(input.files[0]);
        }
      }

      $("#uploadBtn").change(function(){
        readURL(this);
      });
    </script>
  </body>
</html>
