<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <?php $this->load->view('layouts/favico'); ?>
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">

    <title><?php echo $judul ?></title>

    <!-- Bootstrap Core CSS -->
    <link href="<?= base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?= base_url('assets/css/hasil-kost.css') ?>" rel="stylesheet">
    <link rel="stylesheet" href="<?= base_url('assets/css/navbar-main.css') ?>">
    <link href="<?= base_url('assets/css/normalize.css') ?>" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?= base_url('assets/font-awesome/css/font-awesome.min.css') ?>" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,500,700,300' rel='stylesheet' type='text/css'>

    <!-- jQuery -->
    <script src="<?= base_url('assets/js/jquery.js') ?>"></script>

    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>

      <header>
        <?php $this->load->view('layouts/navbar-main'); ?>
      </header>
  
    <div class="container-fluid">
      <div class="row pd-rl-5">
        <div class="col-md-2 pd-rl-5 hidden-xs hidden-sm" >
          <div class="col-md-12" id="filter-md">
            <h2>Cari kost</h2>
            <hr class="hr-primary">
            <?= form_open('cari', array('method' => 'get')) ?>
              <div class="form-group">
                <input type="search" name="q" class="form-control" placeholder="cari kost..." autofocus>
              </div>
              <?= form_submit('', 'Cari', array('class' => 'btn btn-block', 'id' => 'cari-filter')); ?>
            <?= form_close(); ?>
          </div> <!-- /#filter -->
        </div>
        <div class="col-md-2 pd-rl-5 visible-xs visible-sm hidden-md" >
          <div class="col-md-12" id="filter-sm-xs">
            <h2>Cari kost</h2>
            <hr class="hr-primary">
            <?= form_open('cari', array('method' => 'get')) ?>
              <div class="form-group">
                <input type="search" name="q" class="form-control" placeholder="cari kost..." autofocus>
              </div>
              <?= form_submit('', 'Cari', array('class' => 'btn btn-block', 'id' => 'cari-filter')); ?>
            <?= form_close(); ?>
          </div> <!-- /#filter -->
        </div>
        <div class="col-md-10 pd-rl-5" >
          <div class="col-md-12" id="konten">
            <h2>Hasil pencarian: <span>"<?= $keyword ?>"</span></h2>
            <hr class="hr-primary">
            <div id="kosts-list" class="clearfix">
            <?php foreach($kosts as $k): ?>
              <div class="col-md-3 pd-rl-5">
                <a href="<?= base_url('detail/'.$k->id_kost) ?>">
                  <div class="kost-item thumbnail">
                    <p class="mg-tb-10"><?= $k->nama_kost ?></p>
                    <img src="<?= base_url('assets/images/kosts/'.$k->foto_kost) ?>" class="image-responsive" alt="">
                    <div class="box">
                      <p><b>Rp.</b> <?= $k->harga ?> / <?= substr($k->jenis_kost, 0, -2) ?></p>
                    </div>
                  </div>
                </a>
              </div>
            <?php endforeach; ?>
            </div> <!-- /#kost-list -->
          </div><!--  /#konten -->
        </div> <!-- /.col-md-10 -->
      </div> <!-- /row -->
    </div><!--  /container-fluid -->

    <!-- footer -->
    <div class="container-fluid">
      <?php $this->load->view('layouts/footer-main'); ?>
    </div>
    <!-- /footer -->
  </body>
</html>
