<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="apple-touch-icon" sizes="57x57" href="<?= base_url('assets/images/favico/apple-icon-57x57.png'); ?>">
    <link rel="apple-touch-icon" sizes="60x60" href="<?= base_url('assets/images/favico/apple-icon-60x60.png'); ?>">
    <link rel="apple-touch-icon" sizes="72x72" href="<?= base_url('assets/images/favico/apple-icon-72x72.png'); ?>">
    <link rel="apple-touch-icon" sizes="76x76" href="<?= base_url('assets/images/favico/apple-icon-76x76.png'); ?>">
    <link rel="apple-touch-icon" sizes="114x114" href="<?= base_url('assets/images/favico/apple-icon-114x114.png'); ?>">
    <link rel="apple-touch-icon" sizes="120x120" href="<?= base_url('assets/images/favico/apple-icon-120x120.png'); ?>">
    <link rel="apple-touch-icon" sizes="144x144" href="<?= base_url('assets/images/favico/apple-icon-120x120.png'); ?>">
    <link rel="apple-touch-icon" sizes="152x152" href="<?= base_url('assets/images/favico/apple-icon-152x152.png'); ?>">
    <link rel="apple-touch-icon" sizes="180x180" href="<?= base_url('assets/images/favico/apple-icon-180x180.png'); ?>">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?= base_url('assets/images/favico/android-icon-192x192.png'); ?>">
    <link rel="icon" type="image/png" sizes="32x32" href="<?= base_url('assets/images/favicon-32x32.png'); ?>">
    <link rel="icon" type="image/png" sizes="96x96" href="<?= base_url('assets/images/favicon-96x96.png'); ?>">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= base_url('assets/images/favicon-16x16.png'); ?>">
    <link rel="manifest" href="<?= base_url('assets/manifest.json'); ?>">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">

    <title><?php echo $judul ?></title>

    <!-- Bootstrap Core CSS -->
    <link href="<?= base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?= base_url('assets/css/tentang-kami.css') ?>" rel="stylesheet">
    <link rel="stylesheet" href="<?= base_url('assets/css/navbar-main.css') ?>">
    <link href="<?= base_url('assets/css/normalize.css') ?>" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?= base_url('assets/font-awesome/css/font-awesome.min.css') ?>" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,500,700,300' rel='stylesheet' type='text/css'>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!-- jQuery -->
    <script src="<?= base_url('assets/js/jquery.js') ?>"></script>

    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>

    <header class="container-fluid">
      <?php $this->load->view('layouts/navbar-main') ?>  
    </header>

      <div class="container">
        <div id="main-section" class="col-md-12">
          <div id="tentang" class="col-md-8 col-md-offset-2">
            <h1>Tentang Kami</h1>
            <hr class="hr-about">
            <p>
              Tilde franzen photo booth beard pop-up, cardigan tattooed gastropub brunch. IPhone vinyl blue bottle master cleanse cold-pressed gastropub, retro distillery man bun mustache polaroid irony. Quinoa twee migas plaid, brooklyn cardigan kogi wolf 90's fingerstache pinterest four dollar toast. Trust fund meditation keffiyeh letterpress iPhone yuccie. Chambray wayfarers kale chips, readymade poutine jean shorts slow-carb bespoke messenger bag. Yuccie synth kinfolk trust fund, gluten-free brunch salvia ramps 3 wolf moon gochujang. Synth fap sriracha intelligentsia flexitarian gochujang.
            </p>
          </div>

          <div id="tim" class="col-md-12">
            <h1>Tim Kami</h1>
            <hr class="hr-about">
            <div class="tim-member col-md-6 col-sm-6 col-xs-12">
              <img src="<?= base_url('assets/images/tim/01.jpg') ?>" alt="achmiral" class="img-responsive img-circle">
              <p class="posisi">Designer, Developer</p>
              <p class="nama">Miral Achmed</p>
              <span>
                <a href="#"><i class="fa fa-twitter-square" aria-hidden="true"></i></a>
                <a href="#"><i class="fa fa-facebook-square" aria-hidden="true"></i></a>
                <a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>
              </span>
            </div>
            <div class="tim-member col-md-6 col-sm-6 col-xs-12">
              <img src="<?= base_url('assets/images/tim/04.jpg') ?>" alt="achmiral" class="img-responsive img-circle">
              <p class="posisi">Developer</p>
              <p class="nama">Ridwan Nugroho</p>
              <span>
                <a href="#"><i class="fa fa-twitter-square" aria-hidden="true"></i></a>
                <a href="#"><i class="fa fa-facebook-square" aria-hidden="true"></i></a>
                <a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>
              </span>
            </div>
            <div class="tim-member col-md-6 col-sm-6 col-xs-12">
              <img src="<?= base_url('assets/images/tim/03.jpg') ?>" alt="achmiral" class="img-responsive img-circle">
              <p class="posisi">Developer</p>
              <p class="nama">Ryan Dwi</p>
              <span>
                <a href="#"><i class="fa fa-twitter-square" aria-hidden="true"></i></a>
                <a href="#"><i class="fa fa-facebook-square" aria-hidden="true"></i></a>
                <a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>
              </span>
            </div>
            <div class="tim-member col-md-6 col-sm-6 col-xs-12">
              <img src="<?= base_url('assets/images/tim/02.jpg') ?>" alt="achmiral" class="img-responsive img-circle">
              <p class="posisi">Developer</p>
              <p class="nama">Sugeng Santoso</p>
              <span>
                <a href="#"><i class="fa fa-twitter-square" aria-hidden="true"></i></a>
                <a href="#"><i class="fa fa-facebook-square" aria-hidden="true"></i></a>
                <a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>
              </span>
            </div>
            <div class="clearfix"></div>
          </div>
        </div>
      </div> <!-- /.container -->

      <!-- footer -->
      <div class="container-fluid">
        <?php $this->load->view('layouts/footer-main') ?>
      </div>
      <!-- /footer -->
  </body>
</html>
