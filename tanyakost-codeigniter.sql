-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 21, 2016 at 02:43 PM
-- Server version: 5.5.49-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `tanyakost-codeigniter`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(30) NOT NULL,
  `password` char(32) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `telepon` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_admin` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `email`, `password`, `nama`, `telepon`) VALUES
(3, 'admin@email.com', '827ccb0eea8a706c4c34a16891f84e7b', 'Achmiral', '1234567890');

-- --------------------------------------------------------

--
-- Table structure for table `kost`
--

CREATE TABLE IF NOT EXISTS `kost` (
  `id_kost` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `nama_kost` varchar(30) NOT NULL,
  `jenis_kost` varchar(50) NOT NULL,
  `tipe_kost` varchar(50) NOT NULL,
  `photo` varchar(30) DEFAULT NULL,
  `harga` varchar(20) NOT NULL,
  `deskripsi` text,
  `fasilitas` text,
  `provinsi` varchar(30) NOT NULL,
  `kota_kabupaten` varchar(30) NOT NULL,
  `kecamatan` varchar(30) NOT NULL,
  `jalan` varchar(30) NOT NULL,
  `tanggal_upload` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_kost`),
  KEY `id_user` (`id_user`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `kost`
--

INSERT INTO `kost` (`id_kost`, `id_user`, `nama_kost`, `jenis_kost`, `tipe_kost`, `photo`, `harga`, `deskripsi`, `fasilitas`, `provinsi`, `kota_kabupaten`, `kecamatan`, `jalan`, `tanggal_upload`) VALUES
(4, 21, 'Kos Melati', 'Bulanan', 'putra', NULL, '100000', 'lsdj;fkasjdf;lk', 'laksjdf;alksdfj;', 'Di Yogyakarta', 'Sleman', 'Depok', 'Jl. Nusa Indah, No, 4, Condong', '2016-05-20 04:47:09'),
(5, 22, 'Sakura', 'Putra', 'Harian', NULL, '1.000.000', 'sfasdfasdfasdfasdfasdf', 'asdfasdfasdf, asdfasdfasdf, asdfasdfasdfasd, asdfasdfasdfasd, adsfasdfasfasd, adsfasfasdfasdf, asdfasdfasdf, adsfasdfasdfasd, adsfasdfasdfasd, asdfasdfasd, adsfasfdasd', 'Jogja', 'Bandung', 'depok', 'Jl. Nusa Indah, No. 4', '2016-05-20 15:30:33'),
(12, 16, 'asdfadsf', 'k', '', NULL, '', '', '', '', '', '', '', '2016-05-21 00:41:40'),
(13, 16, 'asdfasdf', '', '', NULL, '', '', '', '', '', '', '', '2016-05-21 00:41:50');

-- --------------------------------------------------------

--
-- Table structure for table `pesan`
--

CREATE TABLE IF NOT EXISTS `pesan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pengirim` varchar(50) NOT NULL,
  `id_user` int(11) NOT NULL,
  `waktu_kirim` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `isi` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_user` (`id_user`),
  KEY `id_user_2` (`id_user`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `pesan`
--

INSERT INTO `pesan` (`id`, `pengirim`, `id_user`, `waktu_kirim`, `isi`) VALUES
(2, 'adsf', 21, '2016-05-20 14:43:30', 'asdf'),
(3, 'tset', 16, '2016-05-21 00:05:52', 'tse'),
(6, 'sdfgsdfg', 21, '2016-05-21 00:11:05', 'sgsdfg');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(30) NOT NULL,
  `password` char(32) NOT NULL,
  `nama` varchar(30) DEFAULT NULL,
  `foto` varchar(50) DEFAULT NULL,
  `provinsi` varchar(50) NOT NULL,
  `kota_kabupaten` varchar(50) NOT NULL,
  `kecamatan` varchar(50) NOT NULL,
  `jalan` varchar(100) NOT NULL,
  `telepon` varchar(14) DEFAULT NULL,
  `tanggal_registrasi` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_user`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `email_2` (`email`),
  KEY `provinsi` (`provinsi`,`kota_kabupaten`,`kecamatan`),
  KEY `jalan` (`jalan`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Tabel untuk pemilik kost' AUTO_INCREMENT=23 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `email`, `password`, `nama`, `foto`, `provinsi`, `kota_kabupaten`, `kecamatan`, `jalan`, `telepon`, `tanggal_registrasi`) VALUES
(16, 'ryan.dwi@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', 'Ryan Septiawan s', NULL, 'D.I. Yogyakarta', 'Yogyakarta', 'Bukit bintang', 'Jl. Nusa Indah, No. 4, Condongcatur', '08689468936 ', '2016-05-19 16:26:02'),
(20, 'coba@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', 'Coba Registrasi', NULL, '', '', '', '', NULL, '2016-05-20 12:19:32'),
(21, 'coba2@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', 'Ridwan Nugroho', NULL, 'Sulawesi Tenggara', '', '', '', '', '2016-05-20 13:05:27'),
(22, 'user@user.com', 'ee11cbb19052e40b07aac0ca060c23ee', 'User', NULL, '', '', '', '', NULL, '2016-05-20 15:20:31');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `kost`
--
ALTER TABLE `kost`
  ADD CONSTRAINT `kost_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pesan`
--
ALTER TABLE `pesan`
  ADD CONSTRAINT `pesan_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
